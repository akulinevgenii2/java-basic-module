import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] array = new double[n];
        for(int i = 0; i < array.length; i++)
            array[i] = scanner.nextDouble();
        System.out.println(searchAVG(array));
    }
    static double searchAVG(double[]array){
        double sum = 0;
        for(double var : array)
            sum += var;
        return sum / array.length;
    }

}
