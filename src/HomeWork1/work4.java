package HomeWork1;

import java.util.Scanner;

public class work4 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        int minutes =  (count / 60) % 60;
        int hour = (count / 60) / 60 % 24;
        System.out.println(hour + " " + minutes);
    }
}
