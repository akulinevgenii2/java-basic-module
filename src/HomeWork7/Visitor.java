package HomeWork7;

public class Visitor {
    private String name;
    private String surname;
    private String bookName;
    private final Integer visitorID;
    private boolean empty;
    private static int numberOfUser = 0;
    private Visitor(){
        ++numberOfUser;
        this.visitorID = numberOfUser;
    }
    public Visitor(String name, String surname){
        this.name = name;
        this.surname = surname;
        this.bookName = "";
        this.empty = true;
        ++numberOfUser;
        this.visitorID = numberOfUser;
    }
    /**
     * Проверка есть ли книга на руках
     */
    public boolean isEmpty(){
        return empty;
    }

    /**
     * Меняет статус принятой или выданной книги
     */

    public void changeEmpty(){
        this.empty = !this.empty;
    }
    public String getName(){
        return this.name;
    }
    public String getSurname(){
        return this.surname;
    }
    public Integer getVisitorID(){
        return this.visitorID;
    }
    public void setBook(String bookName){
        this.bookName = bookName;
    }
    public String getBook(){
        return this.bookName;
    }


    @Override
    public String toString(){
        return this.name + " " + this.surname + " на руках " + (isEmpty()? "нет книг.":("\"" +  this.bookName + "\""));
    }

}
