package HomeWork7;

public class Main {
    public static void main(String[] args) {
        Library myLibrary = new Library();
        myLibrary.addBook("Сказка о золотой рыбке", "Пушкин");
        myLibrary.addBook("Сказка о попе и работнике его балде", "Пушкин");
        myLibrary.addBook("Евгений Онегин", "Пушкин");
        myLibrary.addBook("Кот в сапогах", "Перро");
        myLibrary.addBook("Мёртвые души", "Гоголь");
        myLibrary.addBook("На дне", "Горький");
        myLibrary.addBook("Мёртвые души", "Гоголь");
        System.out.println(myLibrary);

        System.out.println("Поиск по имени книги \"Евгений Онегин\"");
        System.out.println(myLibrary.findByName("Евгений Онегин"));
        System.out.println("Поиск по имени книги \"Тихий Дон\"");
        System.out.println(myLibrary.findByName("Тихий Дон"));

        System.out.println("Поиск по имени автора Пушкин");
        System.out.println(myLibrary.findByAuthor("Пушкин"));

        System.out.println("Поиск по имени автора Лермонтов");
        System.out.println(myLibrary.findByAuthor("Лермонтов"));
        myLibrary.checkStatus();

        System.out.println("\n\nВыдаём книгу!\n\n");

        myLibrary.getBook("Иван", "Петров", "Тихий Дон");
        myLibrary.checkStatus();
        myLibrary.getBook("Фёдор", "Смолов", "Кот в сапогах");
        myLibrary.checkStatus();
        myLibrary.getBook("Фёдор", "Смолов", "На дне");
        myLibrary.checkStatus();
        myLibrary.getBook("Василий", "Иванов", "Кот в сапогах");
        myLibrary.checkStatus();
        myLibrary.getBook("Василий", "Иванов", "На дне");
        myLibrary.checkStatus();

        System.out.println("\n\nУдаляем книгу\n\n");

        myLibrary.delBook("Тихий Дон");
        myLibrary.checkStatus();
        myLibrary.delBook("Кот в сапогах");
        myLibrary.checkStatus();
        myLibrary.delBook("Евгений Онегин");
        myLibrary.checkStatus();

        System.out.println("\n\nВозвращаем книгу\n\n");
        myLibrary.returnBook("Иван", "Смолов", "Кот в сапогах");
        myLibrary.checkStatus();
        myLibrary.returnBook("Фёдор", "Смолов", "Тихий Дон");
        myLibrary.checkStatus();
        myLibrary.returnBook("Фёдор", "Смолов", "Кот в сапогах");
        myLibrary.checkStatus();
    }

}
