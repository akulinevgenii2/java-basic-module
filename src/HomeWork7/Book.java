package HomeWork7;

import java.util.ArrayList;
import java.util.Scanner;

public class Book {
    //private final ArrayList<String>[] listBook = new ArrayList[4];
    private String bookName;
    private String bookAuthor;
    private String bookPlace;
    private Integer visitorID;
    private ArrayList<Integer> bookRang;

    public Book(){
        bookRang = new ArrayList<>();
    }
    public Book(String bookName, String bookAuthor){
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
        this.bookPlace = "в библиотеке";
        this.bookRang = new ArrayList<>();
    }


    @Override
    public String toString(){
        if (this.bookName == null) return "";
        return "Имя книги: \"" + this.bookName + "\", имя автора: " + this.bookAuthor +
                ", сейчас " + bookPlace;
    }

    public void setRating(Integer grade){
        this.bookRang.add(grade);
    }
    public String getBookName(){
        return this.bookName;
    }

    public String getBookAuthor(){
        return this.bookAuthor;
    }
    public String getBookPlace(){
        return this.bookPlace;
    }
    public void setBookPlace(String bookPlace){
        this.bookPlace = bookPlace;
    }
    public void setVisitorID(Integer visitorID){
        this.visitorID = visitorID;
    }
    protected void setMark(){
        System.out.println("Поставьте оценку: ");
        bookRang.add(new Scanner(System.in).nextInt());
        System.out.print("Средняя оценка книги: ");
        Double avg = 0.0;
        for(Integer x : bookRang) avg += x;
        System.out.println(avg / bookRang.size());
    }
}
