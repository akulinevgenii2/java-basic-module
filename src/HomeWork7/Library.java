package HomeWork7;

import java.util.ArrayList;
import java.util.Scanner;

public class Library extends Book {
    private final ArrayList<Visitor> listVisitors = new ArrayList<>();
    private final ArrayList<Book> listBooks = new ArrayList<>();

    /**
     * Выдаёт книгу пользователю, который включается в список, если его в нём нет и у данного пользователя в данный момент
     * нет на руках книги
     *
     * @param userName    имя посетителя
     * @param userSurname фамилия посетителя
     * @param bookName    имя книги
     */
    public void getBook(String userName, String userSurname, String bookName) {
        for (Book book : listBooks) {
            if (book.getBookName().equals(bookName)) {
                getBook(new Visitor(userName, userSurname), book);
                return;
            }
        }
        System.out.println("Книги \"" + bookName + "\" нет в библиотеке.");
    }

    public void getBook(Visitor visitor, Book book) {

        for (Book findBook : listBooks) {
            //проверяем есть ли книга
            if (findBook.getBookName().equals(book.getBookName())) {
                //проверяем выдана ли книга
                if (findBook.getBookPlace().equals("в библиотеке")) {
                    //проверяем есть ли посетитель в списке, если нет добавляем
                    addVisitor(visitor);
                    //проверяем есть ли у посетителя книга
                    for (Visitor user : listVisitors) {
                        if (user.getName().equals(visitor.getName()) &&
                                user.getSurname().equals(visitor.getSurname())) {
                            if (user.isEmpty()) {
                                //выдаём книгу
                                user.setBook(book.getBookName());
                                user.changeEmpty();
                                findBook.setBookPlace(user.getName() + " " + user.getSurname());
                                findBook.setVisitorID(user.getVisitorID());
                                System.out.println("Книга \"" + findBook.getBookName() + "\" была выдана пользователю "
                                        + user.getName() + " " + user.getSurname());
                            } else {
                                System.out.println("У пользователя " + user +
                                        ", чтобы получить \"" + book.getBookName() + "\", нужно сдать старую");
                            }
                            return;
                        }
                    }
                } else {
                    System.out.println("Книга \"" + book.getBookName() + "\" находится на руках у " + findBook.getBookPlace() +
                            ", выдать её не получится");
                    return;
                }
            }
        }
        System.out.println("Книги \"" + book.getBookName() + "\" нет в библиотеке.");
    }

    /**
     * Возвращает книгу в библиотеку только от того пользователя которому она была выдана
     *
     * @param userName    имя пользователя
     * @param userSurname фамилия пользователя
     * @param bookName    имя книги
     */
    public void returnBook(String userName, String userSurname, String bookName) {
        //проверяем есть ли такая книга в библиотеке
        for (Book book : listBooks) {
            if (book.getBookName().equals(bookName)) {
                returnBook(new Visitor(userName, userSurname), book);
                return;
            }
        }
        System.out.println("Книги " + bookName + " нет в библиотеке.");
    }

    public void returnBook(Visitor visitor, Book book) {
        //проверяем есть ли книга в библиотеке
        for (Book findBook : listBooks) {
            if (findBook.getBookName().equals(book.getBookName())) {
                //проверяем есть ли такой посетитель
                for (Visitor user : listVisitors) {
                    if (user.getName().equals(visitor.getName()) &&
                        user.getSurname().equals(visitor.getSurname())) {
                        //посетитель есть, смотрим что у него на руках
                        if (findBook.getBookPlace().equals(user.getName() + " " + user.getSurname())) {
                            //принимает книгу
                            user.setBook("");
                            user.changeEmpty();
                            findBook.setBookPlace("в библиотеке");
                            findBook.setVisitorID(user.getVisitorID());
                            System.out.println("Книга \"" + book.getBookName() + "\" была возвращена " + visitor.getName() + " " +
                                    visitor.getSurname());
                            setMark();
                        }
                        else{
                            System.out.println("У посетителя " + visitor + "\", а вернуть пытается \"" +
                                    book.getBookName() + "\", принять не могу");
                        }
                        return;
                    }
                }
                System.out.println("Не могу принять книгу от посетителя " + visitor.getName() + " " +
                        visitor.getSurname() + ", которого нет в списке посетителей");
                return;
            }
        }
        System.out.println("Книги \"" + book.getBookName() + "\" нет в библиотеке.");
    }

      public void checkStatus(){
          System.out.println("Список книг");
          for(Book book : listBooks){
              System.out.println(book);
          }
          System.out.println("Список читателей");
          for(Visitor visitor : listVisitors){
              System.out.println(visitor);
          }
      }

    public void addBook(String bookName, String bookAuthor){
        addBook(new Book(bookName, bookAuthor));
    }
    public void addBook(Book newBook){
        //проверяем есть ли такая книга
        if (listBooks.contains(newBook)){
            System.out.println("Книга \"" + newBook.getBookName() + "\" уже есть в библиотеке, добавить её не получится");
            return;
        }
        //книги нет, добавляем
        else{
            listBooks.add(newBook);
        }
        System.out.println("В библиотеку добавлена книга \"" + newBook.getBookName() + "\"");
    }
    /**
     * Удаляет книгу, если она есть в библиотеке и не на руках
     * @param bookName имя книги
     */
    public void delBook(String bookName){

        for(Book book:listBooks){
            //проверяем, есть ли книга
            if(book.getBookName().equals(bookName)){
                //проверяем выдана ли она
                if (book.getBookPlace().equals("в библиотеке")){
                    //книга не выдана, удаляем
                    System.out.println("Удалена книга \"" + book.getBookName() + "\".");
                    listBooks.remove(book);
                    return;
                }
                else{
                    //книга выдана
                    System.out.println("Книга \"" + bookName + "\" на руках у " + book.getBookPlace() +
                            " и удалить её не выйдет");
                    return;
                }
            }
        }
        System.out.println("Книги \"" + bookName + "\" нет в библиотеке.");
    }

    /**
     * Поиск книги по имени
     * @param bookName имя книги
     * @return найденную книгу
     */
    public Book findByName(String bookName){

        for(Book book : listBooks){
            //проверяем есть ли такая книга
            if(book.getBookName().equals(bookName)){
                return book;
            }
        }
        System.out.println("Книги " + bookName + " нет в библиотеке");
        return new Book();
    }
    public ArrayList<Book> findByAuthor(String authorName){
        ArrayList<Book>result = new ArrayList<>();
        for(Book book : listBooks){
            //ищем совпадение по автору
            if(book.getBookAuthor().equals(authorName)){
                result.add(book);
            }
        }
        if(result.isEmpty()){
            System.out.print("Книг автора " + authorName + " нет в библиотеке");
        }
        return result;
    }
    public void addVisitor(Visitor visitor){
        for(Visitor user : listVisitors){
            if (user.getName().equals(visitor.getName()) &&
                user.getSurname().equals(visitor.getSurname()))
                return;
        }
        listVisitors.add(visitor);
    }
    @Override
    public String toString(){
        String  result = "";
        for(Book book : listBooks){
            result += book + "\n";
        }
        return result;
    }
}
