    package secondWeek;

    import java.util.Scanner;

    public class Task4 {
        final static int VIP = 125;
        final static int PREMIUM = 110;
        final static int STANDART = 100;
        final static String ERROR = "Не верный номер";
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите номер отеля: ");
            int room = scanner.nextInt();
            if (room < 1 || room > 3) System.out.println(ERROR);
            else if (room == 1) System.out.println(VIP);
            else System.out.println(room == 2 ? PREMIUM : STANDART);
        }
    }
