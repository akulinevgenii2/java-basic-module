package secondWeek;

import java.util.Scanner;

/*
Дано четырехзначное число. Проверить является ли оно палиндромом.
1881 -> true
101 -> true
5081 -> false
 */
public class Task3 {
    static final String NOT_PALINDROM = "не палиндром";
    static final String IS_PALINDROM = "Число палиндром! Ура!";
    static final String ERROR = "Введите корректное число (4 знака)";
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n > 999 && n < 10000) {
            int end = n % 10; // последняя цифра
            int start = n / 1000; //первая цифра
            if (end != start) {
                System.out.println(NOT_PALINDROM);
            } else {
                // System.out.println("n % 100: " + (n % 100));
                // System.out.println("n / 100: " + (n / 100));
                end = (n % 100) / 10;
                start = (n / 100) % 10;
                if (end != start) {
                    System.out.println(NOT_PALINDROM);
                }
                else {
                    System.out.println(IS_PALINDROM);
                }
            }
        } else {
            System.out.println(ERROR);
        }
    }
}
