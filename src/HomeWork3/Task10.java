package HomeWork3;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int space = 0;
        if (n > 2 && n < 10){
            for (int i = 1, t = n, sharp = 1; i <= n; i++, --t, sharp+=2){
                space = (t * 2 - 1) / 2;
                for (; space > 0; --space) System.out.print(" ");
                for (int k =sharp; k>0; k--) System.out.print("#");
                System.out.println();
            }
            for (int i = (n * 2 -1) / 2; i > 0; i--) System.out.print(" ");
            System.out.println("|");
        }
    }
}
