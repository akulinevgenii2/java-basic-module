package HomeWork3;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        int res = 0;
        if (n >0 && n < 1000 && p > 0 && p < 1000){
            for (int i = 0; i < n; i++){
                int temp = scanner.nextInt();
                if (temp < 0 || temp > 1000) break;
                if (temp > p) res += temp;
            }
            System.out.println(res);
        }
    }
}
