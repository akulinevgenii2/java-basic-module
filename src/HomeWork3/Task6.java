package HomeWork3;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int many = scanner.nextInt();
        for (int i = 8; i > 0; i /= 2){
            if (many / i > 0){
                System.out.print(many / i + " ");
                many %= i;
            }else System.out.print("0 ");
        }


    }
}
