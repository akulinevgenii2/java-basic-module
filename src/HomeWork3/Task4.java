package HomeWork3;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int temp = n;
        if (n > 0 && n < 1000000){
            for (int i = 1000000; i > 0; i/=10){
                if (n / i != 0 || temp / i != 0){
                    System.out.println(n/i);
                    n -= n/i * i;
                }
            }
        }
    }
}
