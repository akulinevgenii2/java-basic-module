package HomeWork3;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int res = m;
        if (m > 0 && m < 10 && n > 0 && n < 10){
            for (int i = 2; i <=n; i++)
                res += Math.pow(m,i);
            System.out.println(res);
        }
    }
}
