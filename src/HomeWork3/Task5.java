package HomeWork3;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int res = 0;
        if (m > 0 && m < 10 && n > 0 && n < 10){
            if (m < n) res = m;
            else
                for (int i = n; i <= m; i+=n)
                    res = m - i;
            System.out.println(res);
        }
    }
}
