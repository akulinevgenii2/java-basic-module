package fourthWeek;

import java.util.Arrays;
import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] array = new String[n];
        for(int i = 0; i < n; i++)
            array[i] = scanner.next();
        int m = scanner.nextInt();
        String[] result = new String[n];
        int ind = 0;
        for (int i = 0; i < n; i++)
            if (array[i].length() < m)
                result[ind++] = array[i];
        System.out.println(Arrays.toString(result));
    }
}
