package fourthWeek;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] list = new int[n];
        boolean flag = false;
        for(int i = 0; i < n; i++)
            list[i] = scanner.nextInt();
        for(int i = 0; i < list.length; i++){
            if (list[i] % 2 == 0){
                System.out.println("Чётный элемент: " + list[i]);
                flag = true;
            }
        }
        if (!flag) System.out.println("Чётных элементов нет: " + -1);
    }
}
