package fourthWeek;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        System.out.println(isSorting(arr));
    }

    public static boolean isSorting(int[]inputArray, int length){
        for(int i = 1; i < length; i++)
            if (inputArray[i-1] <= inputArray[i]) {
                return false;
            }
        return true;
    }
    public static boolean isSorting(int[]inputArray){
        return isSorting(inputArray, inputArray.length);
    }
}
