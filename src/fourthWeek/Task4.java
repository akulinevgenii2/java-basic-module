package fourthWeek;

import java.util.Arrays;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] arr1 = fillArray();
        int[] arr2 = fillArray();
        int[] result = unitArray(arr1, arr2);
        sortArrayAndPrint(result);
        unitArrayAndPrint(arr1, arr2);
        mergeTwoArraysAndPrint(arr1, arr2);
    }

    static int[] fillArray(){
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for(int i = 0; i < array.length; i++)
            array[i] = scanner.nextInt();
        return array;
    }
    static int[] unitArray(int[] array1, int[] array2){
        int[] result = new int[array1.length + array2.length];
        int ind = 0;
        for(int i = 0; i < array1.length; i++, ind++)
            result[ind] = array1[i];
        for(int i = 0; i < array2.length; i++, ind++)
            result[ind] = array2[i];
        return result;
    }
    static void unitArrayAndPrint(int[] array1, int[] array2){
        int[] result = new int[array1.length + array2.length];
        System.arraycopy(array1,0,result,0,array1.length);
        System.arraycopy(array2,0,result, array1.length,array2.length);
        Arrays.sort(result);
        System.out.println(Arrays.toString(result));
    }
    static void mergeTwoArraysAndPrint(int[] array1, int[] array2){
        int[] result = new int[array1.length + array2.length];
        int i = 0, j = 0, k = 0;
        while (i < array1.length && j < array2.length) {
            if (array1[i] < array2[j]){
                result[k++] = array1[i++];
            }
            else result[k++] = array2[j++];
            }
        while(i < array1.length)
            result[k++] = array1[i++];
        while(j < array2.length)
            result[k++] = array2[j++];
        System.out.println(Arrays.toString(result));
    }
    /**
     *метод сортирует массив
     * @param array входящий массив
     */
    static void sortArrayAndPrint(int[] array){
        for(int i = 0; i < array.length; i++){
            int currentMin = array[i];
            int minIndex = i;

            for(int j = array.length - 1; j > i ; j--)
                if (currentMin > array[j]){
                    currentMin = array[j];
                    minIndex = j;
                }
            if (minIndex != i){
                array[minIndex] = array[i];
                array[i] = currentMin;
            }
        }
        System.out.println(Arrays.toString(array));
    }

}
