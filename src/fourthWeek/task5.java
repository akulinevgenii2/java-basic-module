package fourthWeek;

import java.util.Arrays;
import java.util.Scanner;

public class task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        //sdvig(arr);
        sdvigLeftArrayCopy(arr);
        System.out.println(Arrays.toString(arr));
    }

    /**
     * Производит цикличныый сдвиг на 1 элемент влево
     * @param source
     */
    static void sdvig(int[]source){
        int temp = source[0];
        for(int i = 1; i < source.length;i++)
            source[i - 1] = source[i];
        source[source.length - 1] = temp;
    }
    static void sdvigLeftArrayCopy(int[]source){
        int temp = source[0];
        System.arraycopy(source,1,source,0,source.length - 1);
        source[source.length - 1] = temp;
    }
}
