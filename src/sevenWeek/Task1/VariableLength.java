package sevenWeek.Task1;

public class VariableLength {
    static int sum(int...numbers){
        int res = 0;
        for(int i = 0; i < numbers.length; i++)
            res += numbers[i];
        return res;
    }
    static boolean findChar(Character ch, String...strings){
        for(int i = 0; i < strings.length; i++)
            if (strings[i].indexOf(ch) != -1)
                return true;
        return false;
    }
    public static void main(String[] args) {
        System.out.println(sum(1));
        System.out.println(sum(1, 2, 3, 4, 5, 6));
        System.out.println(findChar('a', "mother", "python"));
    }
}
