package sevenWeek.Task3;

public enum WeekDays {
    MONDAY(1,"понедельник"),
    TUESDAY(2, "вторник"),
    WEDNESDAY(3, "среда"),
    THURSDAY(4, "четверг"),
    FRIDAY(5, "пятница"),
    SATURDAY(6, "суббота"),
    SUNDAY(7, "воскресенье"),
    NOT_A_DAY(-1,"такого дня нет");
    public final int dayNumber;
    public final String name;
    private static final WeekDays[] ALL_DAYS = values();
    WeekDays(int dayNumber, String name){
        this.dayNumber = dayNumber;
        this.name = name;
    }
    public static WeekDays ofNumber(int dayNumber){
        for(WeekDays day: ALL_DAYS){
            if (day.dayNumber == dayNumber)
                return day;
        }
        return NOT_A_DAY;
    }
    public static WeekDays ofName(String name){
        for(WeekDays day: ALL_DAYS){
            if (day.name.equals(name))
                return day;
        }
        return NOT_A_DAY;
    }
}
