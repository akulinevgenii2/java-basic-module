package sevenWeek.Task2;

public class LazySyncSingletone {
    private static LazySyncSingletone INSTANCE;
    private  LazySyncSingletone(){

    }
    public static synchronized LazySyncSingletone getInstance(){
        if (INSTANCE == null){
            INSTANCE = new LazySyncSingletone();
        }
        return INSTANCE;
    }
}
