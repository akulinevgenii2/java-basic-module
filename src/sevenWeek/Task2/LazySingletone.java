package sevenWeek.Task2;

public class LazySingletone {
    private static LazySingletone INSTANCE;
    private  LazySingletone(){

    }
    public static LazySingletone getInstance(){
        if (INSTANCE == null){
            INSTANCE = new LazySingletone();
        }
        return INSTANCE;
    }
}
