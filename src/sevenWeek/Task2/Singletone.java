package sevenWeek.Task2;

public class Singletone {
    private static final Singletone INSTANCE = new Singletone();
    private  Singletone(){

    }
    public static Singletone getInstance(){
        return INSTANCE;
    }
}
