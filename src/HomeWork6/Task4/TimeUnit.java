package HomeWork6.Task4;

import java.util.SortedMap;

public class TimeUnit {
    private int hours;
    private int minutes;
    private int seconds;


    public TimeUnit(int hours, int minutes, int seconds){
        this.hours = 0;
        this.minutes = 0;
        this.seconds = 0;
        addTime(hours, minutes, seconds);
    }
    public TimeUnit(int hours, int minutes){
        this(hours, minutes,0);
    }
    public TimeUnit(int hours){
        this(hours,0,0);
    }
    public void addTime(int hours, int minutes, int seconds){
        this.seconds += seconds;
        this.minutes += minutes;
        this.hours += hours;
        while (this.seconds < 0){
            this.seconds += 60;
            while(this.minutes - 1 < 0){
                this.minutes +=60;
                while (this.hours - 1 < 0){
                    this.hours += 24;
                }
                this.hours--;
            }
            this.minutes--;
        }
        while (this.seconds >= 60){
            this.seconds -=60;
            while(this.minutes + 1 >= 60){
                this.minutes -=60;
                while (this.hours + 1 >= 24){
                    this.hours -=24;
                }
                this.hours++;
            }
            this.minutes++;
        }
    }
    public void printTime(){
        printTime("");
    }
    public void printTimeAMPM(){
        if (this.hours < 12){
            printTime("am");
        }
        else{
            printTime("pm");
        }
    }
    private void printTime(String time){
        if (time.equals("am") || time.equals("")){
            System.out.print(this.hours < 10 ? "0" + this.hours : this.hours);
            System.out.print(":" + (this.minutes < 10 ? "0" + this.minutes : this.minutes));
            System.out.print(":" + (this.seconds < 10 ? "0" + this.seconds: this.seconds));
            System.out.println(time == "" ? "" : " am");
        }
        else{
            this.hours -= 12;
            System.out.print(this.hours < 10 ? "0" + this.hours : this.hours);
            System.out.print(":" + (this.minutes < 10 ? "0" + this.minutes : this.minutes));
            System.out.print(":" + (this.seconds < 10 ? "0" + this.seconds: this.seconds));
            System.out.println(" pm");
            this.hours += 12;
        }
    }
}
