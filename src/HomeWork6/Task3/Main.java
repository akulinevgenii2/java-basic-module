package HomeWork6.Task3;

import HomeWork6.Task2.Student;

import java.util.Scanner;

public class Main
{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Student[] students = new Student[5];
        for(int i = 0; i < students.length; i++){
            String name = scanner.next();
            String surname = scanner.next();
            int[]grades = new int[10];
            for(int j = 0; j < 10 ; j++){
                grades[j]  = scanner.nextInt();
            }
            students[i] = new Student(name,surname,grades);
        }
        for(int i = 0; i < students.length; i++){
            students[i].printStudent();
        }
        StudentService studentService = new StudentService();
        studentService.bestStudent(students).printStudent();
        studentService.sortBySurname(students);
        for(Student stud : students)
            stud.printStudent();
    }
}
