package HomeWork6.Task3;

import HomeWork6.Task2.Student;

/*
Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
bestStudent() - принимает массив студентов (класс Student из предыдущего задания), возвращает лучшего студента
(т.е. который имеет самый высокий средний балл). Если таких несколько  - вывести любого.
sortBySurname() - принимает массив студентов(класс Student из предыдущего задания) и сортирует его по фамилии
 */
public class StudentService {
    public Student bestStudent(Student[]students){
        double maxAvg = students[0].averageGrade();
        int maxInd = 0;
        for(int i = 0; i < students.length; i++)
            if (students[i].averageGrade() > maxAvg){
                maxAvg = students[i].averageGrade();
                maxInd = i;
            }
        return students[maxInd];
    }
    public void sortBySurname(Student[]students){
        for(int i = 0; i < students.length; i++){
            for(int j = i + 1; j < students.length; j++)
                if (students[i].getSurname().compareToIgnoreCase(students[j].getSurname()) > 0){
                    Student temp = students[i];
                    students[i] = students[j];
                    students[j] = temp;
                }
        }
    }

}
