package HomeWork6.Task5;

public class DayOfWeek {
    private byte day;
    private String dayName;

    public DayOfWeek(byte day, String dayName){
        this.day = day;
        this.dayName = dayName;
    }
    public static DayOfWeek addDay(byte day, String dayName){
        return new DayOfWeek(day, dayName);
    }
    public void printDay(){
        System.out.println(this.day + " " + this.dayName);
    }

}
