package HomeWork6.Task7;

public class TriangleChecker {
    private TriangleChecker(){};
    public static boolean check(int aLine, int bLine, int cLine){
        if (aLine < bLine + cLine && bLine < aLine + cLine && cLine < aLine + bLine)
            return true;
        return false;
    }
}
