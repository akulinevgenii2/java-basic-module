package HomeWork6.Task8;

import java.util.Scanner;

public class Atm {
    private static int numberOfObject;
    private double roublesCurs;
    private double dollarsCurs;
    public Atm(double roublesCurs,double dollarsCurs){
        this.dollarsCurs = getCurrentValue(dollarsCurs,'d');
        this.roublesCurs = getCurrentValue(roublesCurs,'r');
        ++numberOfObject;
    }
    public double getDollarsPerRoubles(double dollars){
        return this.roublesCurs * getCurrentValue(dollars,'D');
    }
    public double getRoublesPerDollars(double roubles){
        return this.dollarsCurs * getCurrentValue(roubles, 'R');
    }
    public int getNumberOfObject(){
        return numberOfObject;
    }
    private double getCurrentValue(double value, char valuta){
        while( value < 0){
            System.out.print("Получено неккоректное значение, пожалуйста введите повторно значение ");
            switch(valuta){
                case 'r' -> System.out.print("курса рубля: ");
                case 'd' -> System.out.print("курса доллара: ");
                case 'R' -> System.out.print("значения рубля: ");
                case 'D' -> System.out.print("значения доллара: ");
            }
            value = new Scanner(System.in).nextDouble();
        }
        return value;
    }
}
