package HomeWork6.Task2;
/*
2.	Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
●	String name — имя студента
●	String surname — фамилия студента
●	int[] grades — последние 10 оценок студента. Их может быть меньше, но не может быть больше 10.
И следующие публичные методы:
●	геттер/сеттер для name
●	геттер/сеттер для surname
●	геттер/сеттер для grades
●	метод, добавляющий новую оценку в grades. Самая первая оценка должна быть удалена, новая должна сохраниться в конце массива (т.е. массив должен сдвинуться на 1 влево).
●	метод, возвращающий средний балл студента (рассчитывается как среднее арифметическое от всех оценок в массиве grades)

 */
public class Student{
    private String name;
    private String surname;
    private final int maxSize = 10;
    private int[]grades = new int[maxSize];
    public Student(String name, String surname){
        this.name = name;
        this.surname = surname;
    }
    public Student(String name, String surname, int[]grades){
        this.name = name;
        this.surname = surname;
        setGrades(grades);
    }
    public void setGrades(int[]grades){
        if (grades.length > maxSize){
            int[]temp = new int [maxSize];
            System.arraycopy(grades,grades.length - maxSize,temp,0,maxSize);
            grades = temp;
        }
        System.arraycopy(this.grades,grades.length,this.grades,0,this.grades.length - grades.length);
        System.arraycopy(grades,0,this.grades,this.grades.length - grades.length,grades.length);
    }
    public int[] getGrades(){
        return this.grades;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void setSurname(String surname){
        this.surname = surname;
    }
    public String getSurname(){
        return this.surname;
    }
    public void addGrade(int grade){
        setGrades(new int[]{grade});
    }
    public double averageGrade(){
        int count = 0;
        double sum = 0;
        for(int i = this.grades.length - 1; i >=0; i--){
            if (this.grades[i] != 0){
                count++;
                sum += this.grades[i];
            }
             else break;
        }
        return sum / count;
    }
    public void printGrades(){
        for(int i = 0; i < this.grades.length;i++)
            System.out.print(this.grades[i] + " ");
        System.out.println();
    }
    public void printStudent(){
        System.out.print(this.name + " " + this.surname + ": ");
        for(int i = 0; i < maxSize; i++)
            System.out.print(this.grades[i] + " ");
        System.out.println();
    }
}