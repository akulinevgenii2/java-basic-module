package HomeWork6.Task2;

public class Main {
    public static void main(String[] args) {
        Student student = new Student("Иван", "Петров");
        student.setGrades(new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14});
        student.printGrades();
        System.out.println(student.averageGrade());
        student.setGrades(new int[]{4,5,6,7,8});
        student.printGrades();
        student.setGrades(new int[]{4,5,6,7,8});
        student.printGrades();
        student.addGrade(9);
        student.printGrades();
        System.out.println(student.averageGrade());

    }
}
