package HomeWork6.Task6;

public class Main {
    public static void main(String[] args) {
        AmazingString string = new AmazingString(new char[]{'h','e','l','l','o'});
        AmazingString string1 = new AmazingString("world");
        for(int i = 0; i < string.getLengthOfString(); i++)
            System.out.println(string.getSymbolFromIndex(i));
        string.printString();
        System.out.println(string.isSubstring(new char[]{'l','o'}));
        System.out.println(string.isSubstring("lo"));
        AmazingString string2 = new AmazingString("   hello");
        string2.printString();
        string2.delStartSpaces();
        string2.printString();
        string2.reverseString();
        string2.printString();
    }
}
