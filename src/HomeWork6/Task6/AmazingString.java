package HomeWork6.Task6;


import java.util.Scanner;

public class AmazingString {
    private char[]str;

    public AmazingString(char[]str){
        this.str = new char[str.length];
        for(int i = 0; i < str.length; i++)
            this.str[i] = str[i];

    }
    public AmazingString(String str){
        this.str = new char[str.length()];
        for(int i = 0; i < str.length(); i++)
            this.str[i] = str.charAt(i);
    }
    public char getSymbolFromIndex(int index){
        while (index < 0 || index > str.length){
            System.out.println("Неверный формат данных, введите новое значение");
            index = new Scanner(System.in).nextInt();
        }
        return str[index];
    }
    public int getLengthOfString(){
        return this.str.length;
    }
    public void printString(){
        for(int i = 0; i < getLengthOfString(); i++)
            System.out.print(this.str[i]);
        System.out.println();
    }

    public boolean isSubstring(char[]str){
        for(int i = 0; i < getLengthOfString(); i++)
            if (this.str[i] == str[0]){
                for(int j = 1, k = i + 1; j < str.length && k < getLengthOfString(); j++, k++){
                    if (this.str[k] != str[j])
                        break;
                    if (j == str.length - 1)
                        return true;
                }
            }
        return false;
    }
    public boolean isSubstring(String str){
        for(int i = 0; i < getLengthOfString(); i++)
            if (this.str[i] == str.charAt(0)){
                for(int j = 1, k = i + 1; j < str.length() && k < getLengthOfString(); j++, k++){
                    if (this.str[k] != str.charAt(j))
                        break;
                    if (j == str.length() - 1)
                        return true;
                }
            }
        return false;
    }

    public void delStartSpaces(){
        int count = 0;
        while(this.str[count] == ' ')
            count++;
        char[]temp = new char[getLengthOfString() - count];
        for(int i = 0; i < getLengthOfString() - count; i++)
            temp[i] = this.str[i + count];
        this.str = temp;
    }
    public void reverseString(){
        char[]temp = new char[getLengthOfString()];
        for(int i = 0, j = getLengthOfString() - 1; i < getLengthOfString(); i++, j--)
            temp[i] = this.str[j];
        this.str = temp;
    }
}
