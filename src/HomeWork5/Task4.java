package HomeWork5;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] array = new int[n][n];
        for(int i = 0; i < array.length; i++)
            for(int j = 0; j <array[i].length; j++)
                array[i][j] = scanner.nextInt();
        int p = scanner.nextInt();
        int[][] result = getResult(array, p);
        for(int i = 0; i < result.length; i++){
            for(int j = 0; j < result[i].length; j++){
                System.out.print(result[i][j]);
                if (j + 1 < result.length)
                    System.out.print(" ");
            }
            System.out.println();
        }
    }

    static int[][] getResult(int[][]source, int val){
        int iInd = 0, jInd = 0;
        boolean isFound = false;
        for(int i = 0; i < source.length; i++){
            for(int j = 0; j < source[i].length; j++){
                if (source[i][j] == val){
                    iInd = i;
                    jInd = j;
                    break;
                }
            }
            if (isFound)
                break;
        }
        int[][]result = new int[source.length - 1][source.length - 1];
        for(int i1 = 0, i2 = 0; i1 < result.length ; i1++, i2++){
            if (i2 == iInd) i2++;
            for(int j1 = 0, j2 = 0; j1 < result.length; j1++, j2++){
                if (j2 == jInd) j2++;
                result[i1][j1] = source[i2][j2];
            }
        }
        return result;
    }
}
