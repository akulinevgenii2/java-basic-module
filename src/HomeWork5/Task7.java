package HomeWork5;

import java.util.Arrays;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        String[][] name = new String[2][n];
        for(int i = 0; i < 2; i++)
            for(int j = 0; j < name[i].length; j++){
                name[i][j] = scanner.next();
            }


        int[][] ball = new int[n][3];
        for(int i = 0; i < ball.length; i++)
            for(int j = 0; j < ball[i].length; j++)
                ball[i][j] = scanner.nextInt();

        double[] avgBall = getResult(ball);
        for(int i = 0; i < 3; i++){
            double max = avgBall[i];
            int maxInd = i;
            for(int j = 0; j < avgBall.length; j++)
                if (max < avgBall[j]){
                    max = avgBall[j];
                    maxInd = j;
                }
            System.out.println(name[0][maxInd] + ": " + name[1][maxInd] + ", " + (int)(avgBall[maxInd] * 10) / 10.0);
            avgBall[maxInd] = 0;
        }
    }

    static double[] getResult(int[][]source){
        double[] result = new double[source.length];
        for(int i = 0; i < source.length; i++){
            for(int j = 0; j < source[i].length; j++)
                result[i] += source[i][j];
            result[i] /= 3.0;
        }
        return result;
    }
}
