package HomeWork5;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] array = new int[8][4];
        for(int i = 0; i < array.length; i++)
            for(int j = 0; j < array[i].length; j++)
                array[i][j] = scanner.nextInt();
        System.out.println(calculate(array));
    }
    static String calculate(int[][]array){
        int[] week = new int[4];
        for(int i = 0; i < array[0].length; i++)
            for(int j = 1; j < array.length; j++)
                week[i] += array[j][i];
        for(int i = 0; i < week.length; i++)
            if (week[i] > array[0][i])
                return "Нужно есть поменьше";
        return "Отлично";
    }
}
