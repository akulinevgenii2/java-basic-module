package HomeWork5;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] array = new int[n][n];
        for(int i = 0; i < array.length; i++)
            for(int j = 0; j < array[i].length; j++)
                array[i][j] = scanner.nextInt();
        System.out.println(isSymmetrically(array));

        }

    static boolean isSymmetrically(int[][]source) {
        for (int i = 0, j = source.length - 1; i < source.length; i++, j--) {
            for (int iInd = i, jInd = j; iInd < source.length; iInd++, jInd--)
                if (source[iInd][j] != source[i][jInd])
                    return false;
        }
        return true;
    }
}
