package HomeWork5;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int[][] arr = new int[m][n];
        //значения
        for (int i = 0; i < arr.length; i++) {
            for(int j = 0; j < arr[i].length; j++){
                arr[i][j] = scanner.nextInt();
            }
        }
        for (int i = 0; i < arr.length; i++) {
            int min = arr[i][0];
            for(int j = 0; j < arr[i].length; j++){
                if (min > arr[i][j])
                    min = arr[i][j];
            }
            System.out.print(min + " ");
        }

    }
}
