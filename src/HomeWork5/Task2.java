package HomeWork5;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] array = new int[n][n];
        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();
        int x2 = scanner.nextInt();
        int y2 = scanner.nextInt();

        for(int i = x1; i <= x2; i++){
            array[y1][i] = 1;
            array[y2][i] = 1;
        }
        for(int i = y1; i < y2; i++){
            array[i][x1] = 1;
            array[i][x2] = 1;
        }


        for(int i = 0; i < array.length;i++){
            for(int j = 0; j < array[i].length; j++){
                System.out.print(array[i][j]);
                if (j < array[i].length - 1)
                    System.out.print(" ");
            }
            System.out.println();
        }

    }
}
