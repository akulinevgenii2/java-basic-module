package HomeWork5;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        getNumber(new Scanner(System.in).nextInt());
    }
    static void getNumber(int n){
        if (n / 10 == 0) {
            System.out.print(n % 10 + " ");
            return;
        }
        getNumber(n / 10);
        System.out.print(n % 10 + " ");
    }

}
