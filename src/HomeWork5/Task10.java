package HomeWork5;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        getNumber(new Scanner(System.in).nextInt());
    }
    static void getNumber(int n){
        if (n / 10 == 0) {
            System.out.print(n % 10);
            return;
        }
        System.out.print(n % 10 + " ");
        getNumber(n / 10);
    }
}
