package HomeWork5;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(getSum(n));
    }

    private static int getSum(int n) {
        if (n <= 0) return 0;
        return n % 10 + getSum(n / 10);
    }
}
