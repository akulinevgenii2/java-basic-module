package HomeWork5;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = 0;
        for (; n <= 4 || n >= 100; n = scanner.nextInt()) ;
        String[][] array = new String[n][n];
        for (int i = 0; i < array.length; i++)
            for (int j = 0; j < array[i].length; j++)
                array[i][j] = "0";
        int x = -1, y = -1;
        while (y < 0 || y >= n)
            y = scanner.nextInt();
        while (x < 0 || x >= n)
            x = scanner.nextInt();
        array[x][y] = "K";


        isInsert(array, x - 1, y + 2);
        isInsert(array, x + 1, y + 2);
        isInsert(array, x + 2, y - 1);
        isInsert(array, x + 2, y + 1);
        isInsert(array, x - 1, y - 2);
        isInsert(array, x + 1, y - 2);
        isInsert(array, x - 2, y - 1);
        isInsert(array, x - 2, y + 1);


        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();
        }
    }
    static void isInsert (String[][] source,int x, int y){
        if (x >= 0 && x < source.length && y >= 0 && y < source[0].length)
            source[x][y] = "X";
    }
}
