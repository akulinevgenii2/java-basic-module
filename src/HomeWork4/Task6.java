package HomeWork4;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] kodMorze = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..",
                "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-",
                "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};
        String[] alfavit = {"А","Б","В","Г","Д","Е","Ж","З","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф",
               "Х","Ц","Ч","Ш","Щ","Ъ","Ы","Ь","Э","Ю","Я"};
        String str = scanner.next();

        for(int i = 0; i < str.length(); i++)
            for(int j = 0; j < alfavit.length;j++)
                if (str.charAt(i) == alfavit[j].charAt(0))
                    System.out.print(kodMorze[j] + " ");
        System.out.println();
    }
}
