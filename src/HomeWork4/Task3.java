package HomeWork4;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for(int i = 0; i < n; i++)
            array[i] = scanner.nextInt();
        int m = scanner.nextInt();
        System.out.println(findInd(array, m));

    }

    static int findInd(int[]source, int val){
        for(int i = 0; i < source.length - 1;i++)
            if (source[i] <= val && source[i + 1] > val) return i + 1;
        return -1;
    }
}
