package HomeWork4;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for(int i = 0; i < array.length; i++)
            array[i] = scanner.nextInt();
        findRepeat(array);
    }

    /**
     * Поиск количества повторов символов
     * @param source исходный массив
     */
    static void findRepeat(int[]source){
        for(int i = 0; i < source.length; i++){
            int count = 1;
            for(int j = i + 1; j < source.length; j++)
                if (source[i] == source[j]) {
                    count++;
                    i++;
                }
                else break;
            System.out.println(count + " " + source[i]);
        }
    }
}
