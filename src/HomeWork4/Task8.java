package HomeWork4;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for(int i = 0; i < array.length; i++)
            array[i] = scanner.nextInt();
        int m = scanner.nextInt();
        int minRazn = Math.abs(m - array[0]);
        int minZnac = array[0];
        int maxOfthat = array[0];
        for(int i = 1; i < array.length; i++)
            if (Math.abs(m - array[i]) <= minRazn){
                minRazn = Math.abs(m - array[i]);
                minZnac = array[i];
                if (maxOfthat < minZnac)
                    maxOfthat = minZnac;
            }
        System.out.println(maxOfthat);

    }
}
