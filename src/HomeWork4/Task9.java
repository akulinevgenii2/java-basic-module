package HomeWork4;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] array = new String[n];
        for(int i = 0; i < array.length; i++)
            array[i] = scanner.next();
        System.out.println(searchDoublecates(array));
    }

    /**
     * ищем совпадение строк
     * @param list массив для поиска
     * @return значение
     */
    static String searchDoublecates(String[]list){
        for(int i = 0; i < list.length; i++){
            for(int j = i + 1; j < list.length; j++)
                if (list[i].length() != list[j].length()) continue;
                else{
                    boolean flag = true;
                    for(int k = 0; k < list[i].length(); k++)
                        if (list[i].charAt(k) != list[j].charAt(k)){
                            flag = false;
                            break;
                        }
                    if (flag) return list[i];
                }
        }
        return "Недосигаемое по условию значение";
    }
}
