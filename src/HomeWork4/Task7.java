package HomeWork4;

import java.util.Arrays;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for(int i = 0; i < n; i++){
            array[i] = scanner.nextInt();
            array[i] = (int)Math.pow(array[i], 2);
        }
        Arrays.sort(array);
        for(int i = 0; i < array.length; i++)
            System.out.print(array[i] + " ");
    }
}
