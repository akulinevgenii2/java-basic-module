package HomeWork4;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[]array = new int[n];
        for(int i = 0; i < n; i++)
            array[i] = scanner.nextInt();
        int m = scanner.nextInt();
        while(m > n) m-=n;
        int[] temp = new int[m];
        int ind = m - 1;
        for(int i = array.length - 1; ind >= 0; i--, ind--)
            temp[ind] = array[i];
        System.arraycopy(array,0,array, m, array.length - m);
        System.arraycopy(temp,0,array,0, temp.length);
        for(int val : array)
            System.out.print(val + " ");
    }
}
