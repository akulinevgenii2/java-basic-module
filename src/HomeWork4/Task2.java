package HomeWork4;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] mass1 = new int[n];
        for(int i = 0; i < mass1.length; i++)
            mass1[i] = scanner.nextInt();

        int k = scanner.nextInt();
        int[] mass2 = new int[k];
        for(int i = 0; i < mass2.length; i++)
            mass2[i] = scanner.nextInt();

        System.out.println(isEqual(mass1, mass2));
    }



    /**
     * Возвращает результат сравнения двух массивов
     * @param mass1 первый массив
     * @param mass2 второй массив
     * @return возвращает true или false
     */

    static boolean isEqual(int[]mass1, int[]mass2){
        if (mass1.length != mass2.length) return false;
        for(int i = 0; i < mass1.length; i++)
            if (mass1[i] != mass2[i]) return false;
        return true;
    }
}
