package thirdweek;

import java.util.Scanner;

public class Task6{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        if (count < 2 && count > 0){
            System.out.println("Max digits: " + scanner.nextInt());
        }
        else if (count >= 2){
            int firstNumber = scanner.nextInt();
            int secondNumber = scanner.nextInt();
            int firstMax = Math.max(firstNumber,secondNumber);
            int secondMax = Math.min(firstNumber, secondNumber);
            for (int n = 2; n < count; n++ ){
                int k =scanner.nextInt();
                if (k > firstMax){
                    secondMax = firstMax;
                    firstMax = k;
                }
                else if (k > secondNumber) secondNumber = k;
            }
            System.out.println("Max digits: " + firstMax + ' ' + secondMax);
        }
        else System.out.println("Неверный формат данных");
    }
}