package thirdweek;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n > 0 && n < 13){
            int result = 1;
            for (int i = 2; i <=n; ++i) result *=i;
            System.out.println("Факториал равен: " + result);
        }
        else System.out.println("Число не в заданном промежутке от 0 до 13");
    }
}
