package sixthWeek.Task1;

public class Bulb {
    private boolean toggle;

    public Bulb(){
        this.toggle = false;
    }
    public Bulb(boolean toogle){
        this.toggle = toogle;
    }
    public void turnOn(){
        this.toggle = true;
    }
    public void turnOff(){
        this.toggle = false;
    }
    public boolean isShining(){
        return this.toggle;
    }
}
