package sixthWeek.Task1;

public class Main {
    public static void main(String[] args) {
        Bulb bulb = new Bulb(true);
        bulb.turnOff();
        System.out.println(bulb.isShining());
    }
}
