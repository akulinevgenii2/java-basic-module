package sixthWeek.Task1;

public class Chandelier {
    private Bulb[] chandelier;

    public Chandelier(int countOfBulbs){
        this.chandelier = new Bulb[countOfBulbs];
        for(int i = 0; i < countOfBulbs; i++)
            chandelier[i] = new Bulb();
    }
    public void turnOn(){
        for(int i = 0; i < this.chandelier.length; i++)
            this.chandelier[i].turnOn();
    }
    public void turnOff(){
        for(int i = 0; i < this.chandelier.length; i++)
            this.chandelier[i].turnOff();
    }
    public boolean isShining(){
        return this.chandelier[0].isShining();
    }
}
