package sixthWeek.Task2;

public class Thermometr {
    private double tempCelsius;
    private double tempFahrenheit;

    public Thermometr(double currentTemperature){
        tempCelsius = currentTemperature;
        tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
    }

    public Thermometr(double currentTemperature, String temperatureUnit){
        if (temperatureUnit.equals("C")){
            tempCelsius = currentTemperature;
            tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
        }
        else if (temperatureUnit.equals("F")){
            tempFahrenheit = currentTemperature;
            tempCelsius = fromFahrenheitToCelsius(currentTemperature);
        }
        else{
            System.out.println("Единица измерения температуры не распознана. По умолчанию = цельсий");
            tempCelsius = currentTemperature;
            tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
        }
    }
    private double fromCelsiusToFahrenheit(double currentTemperature){
        return currentTemperature * 1.8 + 32;
    }
    private double fromFahrenheitToCelsius(double currentTemperature){
        return (currentTemperature -32) / 1.8;
    }

    public double getTempCelsius(){
        return this.tempCelsius;
    }
    public double getTempFahrenheit(){
        return this.tempFahrenheit;
    }
}
