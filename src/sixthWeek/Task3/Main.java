package sixthWeek.Task3;

public class Main {
    public static void main(String[] args) {
        System.out.println(FieldValidator.validateEmail("andy_12@bk.ru"));
        System.out.println(FieldValidator.validatePhone("+79506042358"));
        System.out.println(FieldValidator.validateName("Evgenii"));
        System.out.println(FieldValidator.validateBirthDate("09.03.1990"));
    }
}
