package sixthWeek.Task4;

public class Main {
    public static void main(String[] args) {
        Robot robot = new Robot();
        robot.turnLeft();
        robot.go();
        robot.turnRight();
        robot.go();
        robot.turnRight();
        robot.go();
        robot.turnRight();
        robot.go();
    }
}
