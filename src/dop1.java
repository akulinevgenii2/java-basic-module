import java.util.Scanner;
import static java.lang.Character.*;

public class dop1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String password = scanner.next();
        if (password.length() >= 8) {
            boolean upper = false, lower = false, digit = false, sign = false;
            for (int i = 0; i < password.length(); ++i) {
                if (!upper)
                    upper = isUpperCase(password.charAt(i));
                if(!lower)
                    lower = isLowerCase(password.charAt(i));
                if (!digit)
                    digit = isDigit(password.charAt(i));
                if (!sign)
                    sign = !isLetterOrDigit(password.charAt(i));
                if (upper && lower && digit && sign) {
                    System.out.println("пароль надёжный");
                    return;
                }
            }
        }
        System.out.println("пароль не прошёл проверку");
    }
}
