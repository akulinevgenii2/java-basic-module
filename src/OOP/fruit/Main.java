package OOP.fruit;

public class Main {
    public static void main(String[] args) {
        Fruit fruit = new GoldenDelisious();
        Orange orange = new Orange();
        System.out.println(fruit instanceof Fruit);
        System.out.println(fruit instanceof GoldenDelisious);
        System.out.println(fruit instanceof McIntosh);
        ((Apple)fruit).makeAppleCider();
        GoldenDelisious a = (GoldenDelisious) new Fruit();
        Object o = new Apple();
        Object b = (GoldenDelisious)o;
    }
}
