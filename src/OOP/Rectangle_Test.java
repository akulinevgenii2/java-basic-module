package OOP;

public class Rectangle_Test {
    public static void main(String[] args) {
        Rectangle rectangleOne = new Rectangle(4,40);
        System.out.println("Площадь прямоугольника с шириной " + rectangleOne.width +
                " и высотой " + rectangleOne.height + " = " + rectangleOne.getArea());
        System.out.println("Периметр = " + rectangleOne.getPerimeter());

        Rectangle rectangleTwo = new Rectangle(3.5,35.9);
        System.out.println("Площадь прямоугольника с шириной " + rectangleTwo.width +
                " и высотой " + rectangleTwo.height + " = " + rectangleTwo.getArea());
        System.out.println("Периметр = " + rectangleTwo.getPerimeter());

    }
}
