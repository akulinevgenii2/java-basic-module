package OOP;

public class BMI {
    //имя
    private String name;
    //возраст
    private int age;
    //вес
    private double weight;
    //высота
    private double height;

    /**
     * Конструктор с заданными парамертами
     * @param name имя
     * @param age возраст
     * @param weight вес
     * @param height высота
     */
    public BMI(String name, int age, double weight, double height){
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.height = height;
    }

    /**
     * Конструктор с заданными парамертами
     * @param name имя
     * @param weight вес
     * @param height высота
     */
    public BMI(String name, double weight, double height){
        this.name = name;
        this.weight = weight;
        this.height = height;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getAge(){
        return age;
    }
    public void setAge(int age){
        this.age = age;
    }
    public double getWeight(){
        return weight;
    }
    public void setWeight(double weight){
        this.weight = weight;
    }
    public double getHeight(){
        return height;
    }
    public void setHeight(double height){
        this.height = height;
    }
    /**
     * Возвращает индекс тела
     * @return индекс
     */
    public double getBMI(){
        return Math.round(weight / (height / 100.0 * height / 100.0) * 100) / 100.0;
    }

    /**
     * Анализ полученных данных
     * @return статус
     */
    public String getStatus(){
        double bmi = getBMI();
        if (bmi < 18.5)
            return "Недостаточный вес";
        else if (bmi < 25)
            return "Норма";
        else if (bmi < 30)
            return "Избыточный вес";
        else
            return "Ожирение";
    }
}
