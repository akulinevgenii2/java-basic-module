package OOP;

import java.util.Scanner;

public class MyIntegerTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        MyInteger myInteger = new MyInteger(5);
        char[] str = new char[]{'1','2','3'};
        System.out.println(MyInteger.isOdd(2));
        System.out.println(MyInteger.parseInt(new char[]{'1','2','3'}));
        System.out.println(MyInteger.parseInt("123"));
    }
}
