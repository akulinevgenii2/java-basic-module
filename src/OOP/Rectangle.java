package OOP;

public class Rectangle {
    //значение ширины
    double width = -1;
    //значение высоты
    double height = -1;

    /**
     * Коструктор по умолчанию
     */
    Rectangle(){}

    /**
     * Конструктор с параметрами
     * @param newWidth  новое значение ширины
     * @param newHeight новое значение высоты
     */
    Rectangle(double newWidth, double newHeight){
        width = newWidth;
        height = newHeight;
    }

    /**
     * возвращает значение плозади прямоугольника
     * @return width * height
     */
    double getArea(){
        return width * height;
    }

    /**
     * возвращает периметр
     * @return 2 * (width + height)
     */
    double getPerimeter(){
        return 2 * (width + height);
    }

}
