package OOP;

public class MyString1Test {
    public static void main(String[] args) {
        MyString1 s = new MyString1(new char[] {'a', 'b', 'c'});
        MyString1 s2 = s.substring(1,3);
        s2.printString();
        MyString1 s3 = new MyString1(new char[]{'A','B','c'}).toLowerCase();
        s3.printString();
    }
}
