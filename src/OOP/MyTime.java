package OOP;



public class MyTime {
    private int hour;
    private int minute;
    private int seconds;
    public MyTime(){
        this(System.currentTimeMillis());
    }
    public MyTime(long time){
        hour = (int)(time / 1000 / 60 / 60 % 24);
        minute = (int)(time / 1000 / 60 % 60);
        seconds = (int)(time / 1000 % 60 );
    }
    public MyTime(int hour, int minute, int seconds){
        this.hour = hour % 24;
        this.minute = minute % 60;
        this.seconds = seconds % 60;
    }
    public int getHour(){
        return hour;
    }
    public void setHour(int hour){
        this.hour = hour;
    }
    public int getMinute(){
        return minute;
    }
    public void setMinute(int minute){
        this.minute = minute;
    }
    public int getSeconds(){
        return seconds;
    }
    public void setSeconds(int seconds){
        this.seconds = seconds;
    }
    public void setTime(long elapseTime){
        this.hour = (int)(elapseTime / 1000 / 60 / 60 % 24);
        this.minute = (int)(elapseTime / 1000 / 60 % 60);
        this.seconds = (int)(elapseTime / 1000 % 60 );
    }
    public void showTime(){
        System.out.println(hour + ":" + minute + ":" + seconds);
    }
}
