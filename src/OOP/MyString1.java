package OOP;

import java.util.Arrays;

public class MyString1 {
    private char[] chars;

    public MyString1(char[]chars){
        this.chars = new char[chars.length];
        System.arraycopy(chars,0,this.chars,0,chars.length);
    }
    public char charAt(int index){
        return chars[index];
    }
    public int length(){
        return chars.length;
    }
    public MyString1 substring(int begin,int end){
        char[]temp = new char[end - begin];
        for(int i = begin; i < end;i++)
            temp[i-begin] = chars[i];
        return new MyString1(temp);
    }
    public MyString1 toLowerCase(){
        char[] temp = new char[chars.length];
        for(int i = 0; i < temp.length;i++)
            temp[i] = Character.toLowerCase(chars[i]);
        return new MyString1(temp);
    }

    public void printString(){
        for(int i = 0; i < chars.length; i++)
            System.out.print(chars[i] + " ");
        System.out.println();
    }
}
