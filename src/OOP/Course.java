package OOP;

public class Course {
    private String courseName;
    private String[] students = new String[100];
    private int numberOfStudents;

    public Course(String courseName){
        this.courseName = courseName;
    }
    public String getCourseName(){
        return courseName;
    }
    public void addStudent(String student){
        if (numberOfStudents == students.length){
            String[] temp = students;
            students = new String[students.length * 2 + 1];
            System.arraycopy(temp,0,students,0,temp.length);
        }
        students[numberOfStudents] = student;
        numberOfStudents++;
    }
    public String[] getStudents(){
        String[] temp = new String[numberOfStudents];
        System.arraycopy(students,0,temp,0,numberOfStudents);
        return temp;
    }
    public int getNumberOfStudents(){
        return numberOfStudents;
    }
    public void dropStudent(String student){
        for(int i = 0; i < students.length; i++)
            if (students[i].equals(student)){
                for(int j = i + 1; j < students.length; j++)
                    students[j - 1] = students[j];
                numberOfStudents--;
                break;
            }
    }
    public void clear(){
        for(int i = 0; i < students.length; i++)
            students[i] = null;
        numberOfStudents = 0;
    }
    public void printStudents(){
        for (int i = 0; i < students.length; i++)
            System.out.println(students[i] + ", ");
    }
}
