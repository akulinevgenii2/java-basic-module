package OOP;

public class Stock_Test {
    public static void main(String[] args) {
        Stock stock = new Stock("ПАО Сбербанк", "SBER",281.5, 281.5);
        stock.currentPrice = 282.87;
        System.out.println("Изменение стоимости акций " + stock.name + " равно " + stock.getChangePercent());
    }
}
