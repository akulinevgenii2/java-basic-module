package OOP.DynamicBinding;

public class Test {
    public static void main(String[] args) {
        new A();
        new B();
    }
}
class A {
    int i = 7;
    public A() {
        setI(20);
        System.out.println("i из класса A равно " + i);
    }
    public void setI(int i) {
        this.i = 2 * i;
    }
}
class B extends A {
    public B() {
        System.out.println("i из класса B равно " + i);
    }
    public void setI(int i) {
        this.i = 3 * i;
    }
}
