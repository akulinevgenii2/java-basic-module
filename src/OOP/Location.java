package OOP;

public class Location {
    public int row = 0;
    public int column = 0;
    public double maxValue = 0.0;

    /**
     * Поиск наибольшего элемента в двумерном массиве и его индекса
     * @param a двумерный массив
     * @return Location
     */
    public static Location locateLargest(double[][] a){
        Location location = new Location();
        location.maxValue = a[0][0];
        for(int i = 0; i < a.length; i++)
            for(int j = 0; j < a[i].length; j++)
                if (location.maxValue < a[i][j]){
                    location.maxValue = a[i][j];
                    location.row = i;
                    location.column = j;
                }
        return location;
    }
}
