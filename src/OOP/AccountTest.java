package OOP;

public class AccountTest {
    public static void main(String[] args) {
        Account account = new Account(1122,20000);
        Account.setAnnualInterrestRate(4.5);
        account.withdraw(2500);
        account.deposit(3000);
        System.out.println("баланс: " + account.getBalance() + ", ежемесячные проценты: " + account.getMonthlyInterest()
                            + ", дата создания счёта: " + account.getDateCreated());
    }
}
