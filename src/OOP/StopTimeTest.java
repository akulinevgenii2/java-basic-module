package OOP;

import java.util.Random;

public class StopTimeTest {
    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        System.out.println(stopWatch.getStartTime());
        int[] array = new int[100000];
        Random random = new Random();
        for(int i = 0; i < 100000; i++)
            array[i] = random.nextInt(100000);
        for(int i = 0; i < array.length; i++){
            int minVal = array[i];
            int minInd = i;
            for(int j = i + 1; j < array.length; j++)
                if (minVal > array[j]){
                    minVal = array[j];
                    minInd = j;
                }
            if (minInd != i){
                array[minInd] = array[i];
                array[i] = minVal;
            }
        }
        stopWatch.stop();
        System.out.println(stopWatch.getEndTime());
        System.out.println(stopWatch.getElapsedTime());
    }
}
