package OOP.polymorphism;

import java.util.ArrayList;
import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        String[]array = {"красный", "зелёный", "синий"};
        ArrayList<String>list = new ArrayList<>(Arrays.asList(array));
        list.toArray(array);
    }
}
