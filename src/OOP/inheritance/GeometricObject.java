package OOP.inheritance;

import java.util.Date;

public class GeometricObject {
    private String color = "белый";
    private boolean filled;
    private Date dateCreated;
    private double area, perimeter;

    public GeometricObject(){
        dateCreated = new Date();
    }
    public GeometricObject(String color, boolean filled){
        this.color = color;
        this.filled = filled;
        dateCreated = new Date();
    }
    public String getColor(){
        return this.color;
    }
    public void setColor(String color){
        this.color = color;
    }
    public boolean isFilled(){
        return filled;
    }
    public void setFilled(boolean filled){
        this.filled = filled;
    }
    public Date getDateCreated(){
        return dateCreated;
    }
    public String toString(){
        return "создан " + this.dateCreated + ", цвет: " + this.color + ", заливка: " + this.filled;
    }
    public double getArea(){
        return area;
    }
    public double getPerimeter(){
        return perimeter;
    }
}
