package OOP.inheritance;

public class Main {
    public static void main(String[] args) {
        Object object1 = new Circle(1);
        Object object2 = new Rectangle(1,1);
        displayObject(object1);
        displayObject(object2);
    }

    public static void displayObject(Object object){
        if (object instanceof Circle){
            System.out.println("Площадь круга равна " + ((Circle)object).getArea());
            System.out.println("Диаметр круга равен " + ((Circle)object).getDiameter());
        }else if (object instanceof Rectangle){
            System.out.println("Площадь прямоугольника равна " + ((Rectangle)object).getArea());
        }
    }
}
