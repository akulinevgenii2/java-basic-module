package OOP;

import java.util.Scanner;

public class LoanTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите процентную ставку: ");
        double annualInterestRate = scanner.nextDouble();
        System.out.print("Введите количество лет: ");
        int numberOfYears = scanner.nextInt();
        System.out.print("Введите сумма кредита: ");
        double loanAmount = scanner.nextDouble();
        Loan loan = new Loan(annualInterestRate, numberOfYears, loanAmount);
        System.out.println("Ежемесячный платёж: " + ((int)(loan.getMonthlyPayment() * 100)) / 100.0);
        System.out.println("Полная стоимость кредита: " + ((int)(loan.getTotalPayment() * 100)) / 100.0);
    }
}
