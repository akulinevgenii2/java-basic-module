package OOP;

public class Stock {
    //обозначение акций
    String symbol;
    //наименование акций
    String name;
    //стоимость акций на момент закрытия предыдущего дня
    double previousClosingPrice;
    //стоимость акций в текущий момент
    double currentPrice;

    /**
     * Конструктор с параметрами
      * @param isName наименование акций
     * @param isSymbol обозначение акций
     * @param isPreviousClosingPrice стоимость акций на момент закрытия предыдущего дня
     * @param isCurrentPrice стоимость акций в текущий момент
     */
    Stock(String isName, String isSymbol, double isPreviousClosingPrice, double isCurrentPrice){
        name = isName;
        symbol = isSymbol;
        previousClosingPrice = isPreviousClosingPrice;
        currentPrice = isCurrentPrice;
    }

    /**
     * возвращает процент изменения стоимости акций
     * @return currentPrice - previousClosingPrice
     */
    double getChangePercent(){
        return (int)((currentPrice - previousClosingPrice) * 100) / 100.0;
    }
}
