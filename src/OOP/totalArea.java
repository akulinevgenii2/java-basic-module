package OOP;

public class totalArea {
    public static void main(String[] args) {
        // Объявить массив circleArray
        Circle[] circleArray;

        // Создать объект circleArray
        circleArray = createCircleArray();

        // Отобразить circleArray и общую площадь всех кругов
        printCircleArray(circleArray);
    }
    public static Circle[] createCircleArray(){
        Circle[] circles = new Circle[5];
        for(int i = 0; i < circles.length; i++)
            circles[i] = new Circle(Math.random() * 100);
        return circles;
    }
    public static void printCircleArray(Circle[] circle){
        System.out.println("Радиус\t\tПлощадь");
        for(int i = 0; i < circle.length; i++)
            System.out.println(circle[i].getRadius() + "\t\t" + circle[i].getArea());
        System.out.println("-------------------------------------------------------");
        System.out.println("Общая площадь равна " + sum(circle));
    }
    public static double sum(Circle[] circles){
        double sum = 0.0;
        for(int i = 0; i < circles.length; i++)
            sum += circles[i].getArea();
        return sum;
    }
}
