package OOP;
/*
    MyInteger
    -value: int
    +MyInteger(value:int)
    +getValue(): int
    +isEven(): boolean
    +isEven(value: int): boolean
    +isEven(myInteger: MyInteger): boolean
    +isOdd(): boolean
    +isOdd(value: int): boolean
    +isOdd(myInteger: MyInteger): boolean
    +isPrime(): boolean
    +isPrime(value: int): boolean
    +isPrime(myInteger: MyInteger): boolean
    +equals(value: int): boolean
    +equals(myInteger: MyInteger): boolean
    +parseInt(str: char[]): int
    +parseInt(str: String): int
 */
public class MyInteger {
    int value;
    public MyInteger(int value){
        this.value = value;
    }
    public int getValue(){
        return value;
    }
    public boolean isEven(){
        if (value % 2 == 0) return true;
        return false;
    }
    public static boolean isEven(int value){
        return new MyInteger(value).isEven();
    }
    static public boolean isEven(MyInteger myInteger){
        return myInteger.isEven();
    }
    public boolean isOdd(){
        return !isEven();
    }
    static public boolean isOdd(int value){
        return !(new MyInteger(value).isEven());
    }
    static public boolean isOdd(MyInteger myInteger){
        return !myInteger.isEven();
    }
    public  boolean isPrime(){
        int count = 0;
        for(int i = 1; i < value; i++)
            if (value % i == 0)
                count++;
        if (count == 2) return true;
        return false;
    }
    static public boolean isPrime(int value){
        return new MyInteger(value).isPrime();
    }
    static public boolean isPrime(MyInteger myInteger){
        return myInteger.isPrime();
    }
    public boolean equals(int value){
        if (this.value == value) return true;
        return false;
    }
    public boolean equals(MyInteger myInteger){
        return equals(myInteger.value);
    }
    public static int parseInt(char[] str){
        String s = "";
        for(int i = 0; i < str.length; i++)
            s+=str[i];
        return parseInt(s);
    }
    public static int parseInt(String str){
        int n = 0;
        for(int i = 0; i < str.length(); i++)
            if (Character.isDigit(str.charAt(i))){
                n += str.charAt(i) - 48;
                if (i + 1 < str.length())
                    n *= 10;
            }
        return n;
    }
}
