package OOP;



/*
    UML-диаграмма
    StopWatch
    -startTime: long
    -endTime: long
    +getStartTime(): long
    +getEndTime(): long
    +StopWatch()
    +start(): void
    +stop(): void
    +getElapsedTime(): long
 */
public class StopWatch {
    private long startTime;
    private long endTime;

    /**
     * возвращает значение startTime
     * @return startTime
     */
    public long getStartTime(){return startTime;}
    /**
     * возвращает значение endTime
     * @return endTime
     */
    public long getEndTime(){return endTime;}
    /**
     * Конструктор, инициализирует startTime тукущим временем
     */
    public StopWatch(){
        startTime = System.currentTimeMillis();
    }
    /**
     * устанавливает значение startTime
     */
    public void start(){
        startTime = System.currentTimeMillis();
    }
    /**
     * устанавливает значение endTime
     */
    public void stop(){
        endTime = System.currentTimeMillis();
    }

    /**
     * возвращает прошедшее время на секундомере
     * @return
     */
    public long getElapsedTime(){
        return endTime - startTime;
    }
}
