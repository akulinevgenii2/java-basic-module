package OOP;

import java.util.Scanner;

public class LocationTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите количество строчек и столбцов массива: ");
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        System.out.println("Введите массив:");
        double[][] array = new double[n][m];
        for(int i = 0; i < array.length; i++)
            for(int j = 0; j < array[i].length; j++)
                array[i][j] = scanner.nextDouble();
        Location location = Location.locateLargest(array);
        System.out.println("Наибольший элемент массива, равный " + location.maxValue + ", находится в позиции ("
                            + location.row + ", " + location.column + ")");
    }
}
