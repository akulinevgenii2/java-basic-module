package OOP;
/*
UML
MyTime
-hour: int
-minutes: int
-seconds: int
+MyTime()
+MyTime(time: long)
+MyTime(hour: int, minutes: int, seconds: int)
+getHour(): int
+getMinutes(): int
+getSeconds(): int
+setHour(lo): int
+getMinutes(): int
+getSeconds(): int
+setTime(elapsedTime: long): void
+showTime(): void
 */
public class MyTimeTest {
    public static void main(String[] args) {
        MyTime myTime = new MyTime();
        myTime.showTime();
        MyTime myTime1 = new MyTime(555550000);
        myTime1.showTime();
        MyTime myTime2 = new MyTime(5, 23, 55);
        myTime2.showTime();

    }
}
