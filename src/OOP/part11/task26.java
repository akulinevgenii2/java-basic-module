package OOP.part11;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class task26 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите размер матрицы: ");
        int size = scanner.nextInt();
        int[][]matrix = getValue(size);
        System.out.println("Матрица со случайными значениями равна");
        printMatrix(matrix);
        System.out.print("Индекс строчки с наибольшим кол-вом единиц: ");
        ArrayList<Integer>line = searchLine(matrix);
        printArr(line);
        System.out.print("Индекс столбца с наибольшим кол-вом единиц: ");
        ArrayList<Integer>column = searchColumn(matrix);
        printArr(column);

    }
    public static int[][] getValue(int size){
        int[][] matrix = new int[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                int rand = new Random().nextInt(2);
                matrix[i][j] = rand;
            }
        }

        return matrix;
    }

    public static  void printMatrix(int[][]matrix){
        String result;
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[i].length; j++){
                System.out.print(matrix[i][j]);
                if (j + 1 < matrix[i].length)
                    System.out.print(" ");
            }
            System.out.println();
        }

    }

    public static ArrayList<Integer> searchLine(int[][]matrix){
        ArrayList<Integer>result = new ArrayList<>();
        int max = 0;
        for(int i = 0; i < matrix.length; i++){
            int sum = 0;
            for(int j = 0; j < matrix[i].length; j++)
                sum += matrix[i][j];
            if (sum > max){
                result.clear();
                result.add(i);
                max = sum;
            } else if (sum == max)
                result.add(i);
        }
        return result;
    }
    public static ArrayList<Integer> searchColumn(int[][]matrix){
        ArrayList<Integer>result = new ArrayList<>();
        int max = 0;
        for(int i = 0; i < matrix.length; i++){
            int sum = 0;
            for(int j = 0; j < matrix.length; j++)
                sum += matrix[j][i];
            if (sum > max){
                max = sum;
                result.clear();
                result.add(i);
            } else if (sum == max)
                result.add(i);
        }
        return result;
    }

    public static void printArr(ArrayList<Integer>obj){
        for(int i = 0; i < obj.size(); i++){
            System.out.print(obj.get(i));
            if (i + 1 < obj.size())
                System.out.print(", ");
        }
        System.out.println();
    }
}
