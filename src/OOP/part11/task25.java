package OOP.part11;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class task25 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer>list = new ArrayList<>();
        int value;
        do{
            value = scanner.nextInt();
            if (value != 0)
                list.add(value);
        }while(value != 0);
        System.out.println(list.toString());
        shuffle(list);
        System.out.println(list.toString());
    }
    public static void shuffle(ArrayList<Integer>list){
        for(int i = 0; i < list.size(); i++){
            int index = new Random().nextInt(list.size());
            Integer temp = list.get(i);
            list.set(i, list.get(index));
            list.set(index, temp);
        }
    }
}
