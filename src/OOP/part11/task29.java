package OOP.part11;

import java.util.ArrayList;
import java.util.Scanner;

public class task29 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer>list = new ArrayList<>();
        System.out.print("Введите десять целых чисел: ");
        for(int i = 0; i < 10; i++)
            list.add(scanner.nextInt());
        System.out.print("Несовпадающие целые числа равны ");
        removeDuplicate(list);
        for(int i = 0; i < list.size(); i++)
            System.out.print(list.get(i) + " ");
    }
    public static void removeDuplicate(ArrayList<Integer>list){
        for(int i = 0; i < list.size(); i++)
            for(int j = i + 1; j < list.size(); j++)
                if (list.get(i) == list.get(j)){
                    list.remove(j);
                    j--;
                }
    }
}
