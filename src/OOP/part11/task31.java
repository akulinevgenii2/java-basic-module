package OOP.part11;

import java.util.ArrayList;
import java.util.Arrays;

public class task31 {
    public static void main(String[] args) {
        ArrayList<Character>list= toCharacterArray("Hello");
        System.out.println(list.toString());
        ArrayList<String>list2= new ArrayList<>();
        list2.add("Hello");
        System.out.println(list2.toString());
    }
    public static ArrayList<Character> toCharacterArray(String s){
        ArrayList<Character>result = new ArrayList<>();
        for(int i = 0; i < s.length(); i++)
            result.add(s.charAt(i));
        return result;
    }
}
