package OOP.part11;

import java.util.ArrayList;
import java.util.Scanner;

public class task27 {
    public static void main(String[] args) {
        ArrayList<Integer>list = new ArrayList<>();
        for(int i = 0; i < 5; i++){
            int value = new Scanner(System.in).nextInt();
            list.add(value);
        }
        System.out.println("Полученный массив: ");
        System.out.println(list.toString());
        sort(list);
        System.out.println("отсортированный массив: ");
        System.out.println(list.toString());
    }
    public static void sort(ArrayList<Integer>list){
        for(int i = 0; i < list.size(); i++){
            for(int j = i + 1; j < list.size(); j++){
                if (list.get(i) > list.get(j)){
                    Integer temp = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, temp);
                }
            }
        }
    }

}
