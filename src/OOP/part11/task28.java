package OOP.part11;

import java.util.ArrayList;
import java.util.Scanner;

public class task28 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Double>list = new ArrayList<>();
        double value = 0.0;
        int count = 0;
        do{
            value = scanner.nextDouble();
            if(!list.contains(value)){
                list.add(value);
                ++count;
            }
        }while(count < 5);
        System.out.println(sum(list));
    }
    public static double sum(ArrayList<Double> list){
        double result = list.get(0);
        for(int i = 1; i < list.size(); i++)
            result+= list.get(i);
        return result;
    }
}
