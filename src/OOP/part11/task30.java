package OOP.part11;

import java.util.ArrayList;
import java.util.Scanner;

public class task30 {
    public static void main(String[] args) {
        System.out.print("Введите пять целых чисел для списка1: ");
        ArrayList<Integer>list1 = fillList();
        System.out.print("Введите пять целых чисел для списка2: ");
        ArrayList<Integer>list2 = fillList();
        System.out.print("Объединенный список равен ");
        printList(union(list1, list2));
    }
    public static ArrayList<Integer> union(ArrayList<Integer>list1, ArrayList<Integer>list2){
        for(int i = 0; i <list2.size(); i++)
            list1.add(list2.get(i));
        return list1;
    }

    public static ArrayList<Integer> fillList(){
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer>list = new ArrayList<>();
        for(int i = 0; i < 5; i++)
            list.add(scanner.nextInt());
        return list;
    }
    public static void printList(ArrayList<Integer>list){
        for(int i = 0; i < list.size(); i++)
            System.out.print(list.get(i) + " ");
    }
}
