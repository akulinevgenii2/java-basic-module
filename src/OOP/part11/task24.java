package OOP.part11;

import OOP.Loan;
import OOP.inheritance.Circle;

import java.util.ArrayList;
import java.util.Date;

public class task24 {
    public static void main(String[] args) {
        ArrayList<Object>list = new ArrayList<>();
        list.add(new Loan());
        list.add(new Date());
        list.add("ABC");
        list.add(new Circle());
        for(int i = 0; i < list.size(); i++)
            System.out.println(list.get(i).toString());
    }
}
