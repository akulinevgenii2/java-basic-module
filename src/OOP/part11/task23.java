package OOP.part11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class task23 {
    public static void main(String[] args) {
        ArrayList<Integer>list = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        int value;
        do{
            value = scanner.nextInt();
            if (!list.contains(value) || value != 0)
                list.add(value);
        }while(value != 0);

        System.out.println(max(list));
    }

    public static Integer max(ArrayList<Integer> list){
        if(list == null || list.isEmpty())
            return null;
        Integer max = list.get(0);
        for(int i = 1; i < list.size(); i++)
            if (max < list.get(i))
                max = list.get(i);
        return max;
    }
}
