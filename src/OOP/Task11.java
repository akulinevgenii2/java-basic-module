package OOP;

import java.math.BigInteger;

public class Task11 {
    public static void main(String[] args) {
        BigInteger bigNum = new BigInteger("10000000000000000000000000000000000000000000000000");
        int count = 0;
        while(count < 10){
            if (bigNum.divide(new BigInteger("2")).equals(BigInteger.ZERO)
               || bigNum.divide(new BigInteger("3")).equals(BigInteger.ZERO)){
                ++count;
                System.out.println(bigNum);
            }
        }
    }
}
