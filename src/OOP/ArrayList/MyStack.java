package OOP.ArrayList;

import OOP.inheritance.A;

import java.util.ArrayList;


public class MyStack extends ArrayList {
    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    public int getSize(){
        return size();
    }
    public Object peek(){
        return get(size() - 1);
    }
    public Object pop(){
        Object o = get(size() - 1);
        remove(size() - 1);
        return o;
    }
    public void push(Object o){
        add(o);
    }
    @Override
    public String toString(){
        return "стек: " + super.toString();
    }
}
