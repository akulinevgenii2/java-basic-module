package OOP;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MyDate {
    private long year;
    private long month;
    private long day;
    public MyDate(){
        setDate(new GregorianCalendar().getTimeInMillis());
    }
    public MyDate(long elapsedTime){
        setDate(elapsedTime);
    }
    public MyDate(int year, int month, int day){
        this.year = year;
        this.month = month;
        this.day = day;
    }
    public long getYear(){
        return year;
    }
    public long getMonth(){
        return month;
    }
    public long getDay(){
        return day;
    }
    public void setDate(long elapsedTime){
        GregorianCalendar date = new GregorianCalendar();
        date.setTimeInMillis(elapsedTime);
        year = date.get(Calendar.YEAR);
        month = date.get(Calendar.MONTH);
        day = date.get(Calendar.DAY_OF_MONTH);
    }
}
