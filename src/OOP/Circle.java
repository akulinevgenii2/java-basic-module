package OOP;

/*
    UML-диаграмма
    Circle
    -numberOfObjects: int
    -radius: double
    +Circle()
    +Circle(newRadius:double)
    +getNumberOfObjects():int
    +getArea():double
    +getRadius(): double
    +setRadius(newRadius:double):void
 */
public class Circle {
    //радиус круга
    private double radius;
    //количество созданных объектов
    private static int numberOfObjects = 0;

    /**
     * Создаёт круг радиусом = 1
     */
    public Circle(){
        this(1.0);
    }

    /**
     * Создаёт круг с заданным радиусом
     * @param newRadius
     */
    public Circle(double newRadius){
        this.radius = newRadius > 0 ? newRadius : 0;
        numberOfObjects++;
    }

    /**
     * Возвращает количество созданных объектов
     * @return int
     */
    public static int getNumberOfObjects(){
        return numberOfObjects;
    }

    /**
     * Возвращает площадь
     * @return radius * radius * Math.PI
     */
    public double getArea(){
        return this.radius * this.radius * Math.PI;
    }

    /**
     * Возвращает значение радиуса круга
     * @return double
     */
    public double getRadius(){
        return this.radius;
    }

    /**
     * Установить новое значение для radius
     * @param newRadius новый радиус
     */
    public void setRadius(double newRadius) {
        this.radius = newRadius > 0 ? newRadius : 0;
    }
}
