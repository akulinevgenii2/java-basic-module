package OOP;

import java.util.Date;
/*
        Account
        -id: int
        -balance: double
        -annualInterrestRate: double
        -dateCreated: Date
        +Account()
        +Account(id:int, balance:double)
        +getID(): int
        +setID(id:int): void
        +getBalance(): double
        +setBalance(balance:double): void
        +getAnnualInterestRate(): double
        +setAnnualInterestRate(newAnnualInterrestRate: double): void
        +getDateCreated(): Date
        +withdraw(sum: double): void
        +deposit(sum: double): void
 */
public class Account {
    //идентификатор
    private int id = 0;
    //баланс
    private double balance = 0.0;
    //годовая процентная ставка
    private static double annualInterrestRate = 0.0;
    //дата открытия счёта
    private Date dateCreated;

    public Account(){
        this(0,0.0);
    }
    public Account(int id, double balnce){
        this.id = id;
        this.balance = balnce;
        dateCreated = new Date();
    }
    public int getID(){
        return this.id;
    }
    public void setID(int id){
        this.id = id;
    }
    public double getBalance(){
        return this.balance;
    }
    public void setBalance(double balance){
        this.balance = balance;
    }
    public static double getAnnualInterrestRate(){
        return Account.annualInterrestRate;
    }
    public static void setAnnualInterrestRate(double AnnualInterrestRate){
        Account.annualInterrestRate = AnnualInterrestRate;
    }
    public Date getDateCreated(){
        return dateCreated;
    }
    public double getMonthlyInterest(){
        return (balance * annualInterrestRate / 12.0) / 100;
    }
    public void withdraw(double amount){
        this.balance -= amount;
    }
    public void deposit(double amount){
        this.balance += amount;
    }

}
