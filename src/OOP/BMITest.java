package OOP;

import java.util.Scanner;

public class BMITest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        BMI bmi1 = new BMI("Сергей Иванович П.", 18, 60, 178);
        System.out.println("BMI для " + bmi1.getName() + " равно "
                + bmi1.getBMI() + " " + bmi1.getStatus());

        BMI bmi2 = new BMI("Андрей Петрович Н.", 100, 178);
        System.out.println("BMI для " + bmi2.getName() + " равно "
                + bmi2.getBMI() + " " + bmi2.getStatus());
    }
}
