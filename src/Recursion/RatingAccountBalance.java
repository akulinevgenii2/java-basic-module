package Recursion;

import java.util.Scanner;

public class RatingAccountBalance {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfRecords; //количество данных
        Object[][] clients; //массив данных о клиенте

        //Получить количество клиентов
        System.out.println("Введите количество клиентов");
        numberOfRecords = scanner.nextInt();
        //инициализация массива 4 колонки: фамилия имя отчество баланс счёта
        clients = new Object[numberOfRecords][4];

        for(int i = 0; i < numberOfRecords; i++){
            System.out.print("Введите фамилию: ");
            clients[i][0] = scanner.next();
            System.out.print("Введите имя: ");
            clients[i][1] = scanner.next();
            System.out.print("Введите отчество: ");
            clients[i][2] = scanner.next();
            System.out.print("Введите сумму на счёте: ");
            clients[i][3] = scanner.nextDouble();
        }
        //сортировка методом пузырька
        for(int i = 0; i < clients.length; i++)
            for(int j = 0; j < clients.length - 1; j++){
                //преобразование объекта типа Object в объект типа Double
                //преобразование к соответствующему примитиву double
                double firstInPair = (Double)clients[j][3];
                double secondInPair = (Double)clients[j + 1][3];
                if (firstInPair > secondInPair)
                    swap(clients, j, j + 1);
            }
        //Вывести отсортированные данные пользователей
        System.out.println("Данные клиентов в порядке увеличения баланса счёта:");
        for(int i = 0; i < clients.length; i++){
            //Формируем строку по каждому клиенту:
            //фамилия
            String surnameWithInitials = clients[i][0] + " ";
            //имя
            surnameWithInitials += clients[i][1].toString().substring(0,1) + ".";
            //отчество
            surnameWithInitials += clients[i][2].toString().substring(0,1) + ".";
            //баланс счёта
            surnameWithInitials += " " + clients[i][3];
            System.out.println(surnameWithInitials);
        }
    }
    static void swap (Object[][] array, int i, int j){
        for( int k = 0; k < array[i].length; k++){
            Object buff = array[i][k];
            array[i][k] = array[j][k];
            array[j][k] = buff;
        }
    }
}
