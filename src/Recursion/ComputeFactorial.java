package Recursion;

import java.util.Scanner;

public class ComputeFactorial {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(forFactorial(n));
        System.out.println(factorial(n));
    }
    static long forFactorial(int n){
        long result = 1;
        for(int i = 2; i <= n; i++)
            result *=i;
        return result;
    }
    static long factorial(int n){
        if (n == 0) return 1;
        return n * factorial(n - 1);
    }
}
