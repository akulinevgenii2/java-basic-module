package Recursion;

import java.util.Scanner;

public class RecursingPalindromeUsingSubstring {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("ропот - это палиндром? " + isPalindrome("ропот"));
        System.out.println("ропот - это палиндром? " + loopPalindrome("ропот"));
        System.out.println("топот - это палиндром? " + isPalindrome("топот"));
        System.out.println("топот - это палиндром? " + loopPalindrome("топот"));
        System.out.println("я - это палиндром? " + isPalindrome("я"));
        System.out.println("я - это палиндром? " + loopPalindrome("я"));
        System.out.println("ара - это палиндром? " + isPalindrome("ара"));
        System.out.println("ара - это палиндром? " + loopPalindrome("ара"));
        System.out.println("ар - это палиндром? " + isPalindrome("ар"));
        System.out.println("ар - это палиндром? " + loopPalindrome("ар"));
    }

    public static boolean isPalindrome(String str){
        if (str.length() <= 1) return true;
        else if (str.charAt(0) != str.charAt(str.length() - 1)) return false;
        else return isPalindrome(str.substring(1, str.length() - 1));
    }

    public static boolean loopPalindrome(String str){
        if(str.length() <= 1) return true;
        for(int i = 0; i < str.length(); i++)
            if (str.charAt(i) != str.charAt(str.length() - 1 - i)) return false;
        return true;
    }
}
