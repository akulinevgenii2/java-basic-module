package Recursion;

import java.util.Scanner;

public class ComputeFibbanacci {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Получить индекс числа Фибоначчи
        System.out.print("Введите индекс числа Фибоначчи: ");
        int index = input.nextInt();

        // Найти и отобразить число Фибоначчи
        System.out.println("Число Фибоначчи с индексом "
                + index + " равно " + fib(index));
        // Найти и отобразить число Фибоначчи
        System.out.println("Число Фибоначчи с индексом "
                + index + " равно " + fibbanacci(index));
    }

    /**
     * Находит число Фибоначчи
     */
    public static long fib(long index) {
        if (index == 0) // простой случай
            return 0;
        else if (index == 1) // простой случай
            return 1;
        else  // упрощение и рекурсивные вызовы
            return fib(index - 1) + fib(index - 2);
    }

    static long fibbanacci(long index){
        long[] mass = new long[(int)index + 1];
        mass[0] = 0;
        mass[1] = 1;
        for(int i = 2; i <= index; i++){
                mass[i] = mass[i - 1] + mass[i - 2];
        }
        return mass[mass.length - 1];
    }
}
