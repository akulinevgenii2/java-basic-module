package Recursion;

import java.util.Scanner;

public class MaskingCardNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //получить номер карты
        System.out.println("Введите номер карты");
        String cardNumber = scanner.next();
        //маскировка карты через рекурсию
        String updatedCardNumber = maskCharacters(cardNumber, 4);
        //вывести маскировонную сроку
        System.out.println("Маскированный номер банковской карты: " + updatedCardNumber);
    }

    /**
     * Маскировка номера карты через рекурсию
     * @param str исходная строка
     * @param numberFromEnd количество незамаскированных символов в конце
     * @return маскированную строку
     */
    static String maskCharacters(String str, int numberFromEnd){
        if (str.length() <= numberFromEnd || numberFromEnd < 0) return str;
        return "*" + maskCharacters(str.substring(1), numberFromEnd);
    }
}
