package Recursion;

import java.util.Arrays;
import java.util.Scanner;

public class RecursiveBinarySearch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] sourse = new int[n];
        fillArray(sourse);
        printArray(sourse);
        Arrays.sort(sourse);
        printArray(sourse);
        double key = scanner.nextDouble();
        System.out.println(binarySearch(sourse, key));
    }

    /**
     * Заполняет массив случайными значениями
     * @param array double массив
     */
    static void fillArray(int[]array){
        for(int i = 0; i < array.length; i++)
            array[i] = (int)(Math.random() * 100);
    }

    /**
     * Выводит содержимое массива на экран
     * @param array double массив
     */
    static void printArray(int[]array){
        for(int i = 0; i < array.length; i++)
            System.out.print(array[i] + " ");
        System.out.println();
    }

    /**
     * Двоичный поиск через рекурсию
     * @param array исходный массив
     * @param key проверочные значение
     * @return индекс найденного элемента или -ind - 1;
     */
    static int binarySearch(int[]array, double key){
        int startInd = 0;
        int endInd = array.length - 1;
        return binarySearch(array, key, startInd, endInd);
    }

    /**
     * Двоичный поиск через рекурсию
     * @param array исходный массив
     * @param key проверочные значение
     * @param startInd начальный индекс
     * @param endInd конечный индекс
     * @return индекс найденного элемента или -ind - 1;
     */
    static int binarySearch(int[]array, double key, int startInd, int endInd){
        if (startInd > endInd) return -startInd - 1;
        int mid = (startInd + endInd )/ 2;
        if (key == array[mid]) return mid;
        else if (key > array[mid]) return binarySearch(array, key , mid + 1, endInd);
        else return binarySearch(array, key, startInd, mid - 1);
    }
}
