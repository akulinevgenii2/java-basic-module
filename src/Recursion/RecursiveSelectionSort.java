package Recursion;

import java.util.Scanner;
//Рекурсивная сортировка методом выбора
public class RecursiveSelectionSort {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] sourse = new double[n];
        fillArray(sourse);
        printArray(sourse);
        sort(sourse);
        printArray(sourse);
    }

    /**
     * Заполняет массив случайными значениями
     * @param array double массив
     */
    static void fillArray(double[]array){
        for(int i = 0; i < array.length; i++)
            array[i] = Math.random() * 100;
    }

    /**
     * Выводит содержимое массива на экран
     * @param array double массив
     */
    static void printArray(double[]array){
        for(int i = 0; i < array.length; i++)
            System.out.print(array[i] + " ");
        System.out.println();
    }

    static void sort(double[]array){
        sort(array, 0, array.length);
    }
    static void sort(double[]array, int startIndex, int endIndex){
        if (startIndex < endIndex){
            double minValue = array[startIndex];
            int minInd = startIndex;
            for(int i = startIndex; i < endIndex; i++)
                if (minValue > array[i]){
                    minValue = array[i];
                    minInd = i;
                }
            if (startIndex != minInd){
                array[minInd] = array[startIndex];
                array[startIndex] = minValue;
            }
            sort(array, startIndex + 1, endIndex);
        }

    }
}
