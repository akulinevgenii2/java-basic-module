package firstWeek;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double s = scanner.nextDouble();
        double d,l;
        d = Math.sqrt(s * 4 / Math.PI);
        l = d * Math.PI;
        System.out.println("Длина окружности l: " + l);
        System.out.println("Диаметр окружности d: " + d);
    }
}
