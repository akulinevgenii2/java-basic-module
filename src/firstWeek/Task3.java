package firstWeek;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();
        double y = scanner.nextDouble();

        double res = Math.sqrt(x * x + y * y);
        System.out.println("Результат работы программы: " + res);
    }
}
