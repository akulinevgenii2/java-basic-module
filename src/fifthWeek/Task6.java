package fifthWeek;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] array = new int[n][n];

        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                if ((i + j) % 2 != 0) array[i][j] = 1;
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }

    }
}
