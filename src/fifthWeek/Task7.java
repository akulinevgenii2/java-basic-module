package fifthWeek;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        //заполняем значениями
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] array = new int[n][n];
        for(int i = 0; i < n; i++)
            for(int j = 0; j < n; j++)
                array[i][j] = scanner.nextInt();
        int p = scanner.nextInt();
        //обнуление
        for(int i = 0; i < n; i++)
            for(int j = 0; j < n; j++){
                if (array[i][j] == p){
                    fillWithZero(array, n, i, j, p);
                }
            }
        //результат
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();
        }
    }

    private static void fillWithZero(int[][] array, int n, int currI, int currJ, int p) {
        for(int k = 0; k < n; k++){
            if (k != currI) array[k][currJ] = 0;
            if (k != currJ) array[currI][k] = 0;
        }
    }
}
