package fifthWeek;

import java.util.Scanner;
//Поиск факториала числа n рекурсивно
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int factRec = factorial(n);
        System.out.println("Факториал в рекурсии: " + factRec);
        int factTailRec = factorialTail(n, 1);
        System.out.println("Факториал в хвостовой рекурсии: " + factTailRec);
    }
    static int factorial(int input){
        if (input <= 1) return 1;
        return input * factorial(input - 1);
    }
    static int factorialTail(int input, int result){
        if (input <= 1) return result;
        return factorialTail(input - 1, input * result);
    }
}
