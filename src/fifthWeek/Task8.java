package fifthWeek;

import java.util.Random;
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int[][] arr = new int[n][m];
        Random random = new Random();
        //значения
        for (int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++){
                arr[i][j] = random.nextInt(2);
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        boolean answer = false;
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m - 1; j++)
                if (arr[i][j] == 0 && arr[i][j + 1] == 0){
                        answer = true;
                        break;
                }
            if(answer)
                break;
        }
        System.out.println(answer);
    }
}
