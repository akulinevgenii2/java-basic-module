package fifthWeek;

import java.util.Random;
import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int[][] arr = new int[n][m];
        Random random = new Random();
        //значения
        for (int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++){
                arr[i][j] = random.nextInt(10);
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        int[][] result = new int[m][n];
        for(int i = 0; i < arr.length; i ++){
            for(int j = 0; j < arr[i].length; j++){
                    result[j][i] = arr[i][j];
            }
        }
        System.out.println();
        for (int i = 0; i < result.length; i++) {
            for(int j = 0; j < result[i].length; j++){
                System.out.print(result[i][j] + " ");
            }
            System.out.println();
        }

    }
}
