package fifthWeek;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println("Сумма чисел от a до b: " + sumRecursively(a,b));
    }

    private static int sumRecursively(int a, int b) {
        if (b == 0) return a;
        return sumRecursively(a + 1, b - 1);
    }
}
