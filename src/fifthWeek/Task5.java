package fifthWeek;

import java.util.Arrays;
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int[][] arr = new int[n][m];
        //значения
        for (int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++)
                arr[i][j] = scanner.nextInt();
        }
        for(int[] ints: arr)
            System.out.println(Arrays.toString(ints));
        int[] result = new int[m];
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++)
                result[i] = arr[j][i];
        }
        System.out.println(Arrays.toString(result));
    }
}
