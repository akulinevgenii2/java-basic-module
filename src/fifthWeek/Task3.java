package fifthWeek;

import java.util.Scanner;
/*
    развернуть строку рекурсивно.
    abcde -> edcba
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();
        String result = reverseString(input);
        System.out.println(result);
        System.out.println(reverseString(input,input.length()));
    }

    public static String reverseString(String source){
        if (source.isEmpty()) return source;
        return source.charAt(source.length() - 1) + reverseString(source.substring(0,source.length() - 1));
    }
    public static String reverseString(String source, int endInd){
        if (source.isEmpty()) return source;
        if (endInd > 0) return source.charAt(endInd - 1) + reverseString(source, endInd - 1);
        else return "";
    }
}
