package fifthWeek;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while(true){
            int n = scanner.nextInt();
            System.out.println(n + " -> " + checkPowerOfTwoRecursive(n));
        }

    }

    static boolean checkPowerOfTwoRecursive (int input){
        if (input == 2 || input == 1) return true;
        if (input <= 0 || input % 2 != 0) return false;
        return checkPowerOfTwoRecursive(input / 2);
    }
}
